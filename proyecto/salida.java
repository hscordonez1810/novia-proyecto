package proyecto;
import javax.swing.JPanel;
import java.awt.Color;
import java.awt.Font;
import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.BorderFactory;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import javax.swing.border.Border;
import javax.swing.SwingConstants;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class salida extends JPanel {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private static Color BLUE_COLOR = Color.decode("#079992");
    private static Color PANEL_MOSTAZA_COLOR = Color.decode("#FFDB58");
    private static Color GREEN_COLOR = Color.decode("#4cd137");
    private static Font MY_FONT = new Font("", Font.BOLD, 22);
    private JLabel prompt = new JLabel("¿QUIERES SER MI NOVIA?");
    private JLabel thanks = new JLabel("<html><center>Sabía que dirías <br> que si te amo ♡</center></html>");
    private JButton yes = new JButton("SI");
    private JButton no = new JButton("NO");
    private static Border BORDER = BorderFactory.createLineBorder(Color.BLACK, 3);

    public salida(){
        setLayout(null);
        setBackground(salida.PANEL_MOSTAZA_COLOR);

        add(prompt);
        prompt.setBounds(80, 20, 400, 100);
        prompt.setFont(salida.MY_FONT);

        add(thanks);
        thanks.setBounds(150, 130, 160, 90);
        thanks.setBorder(salida.BORDER);
        thanks.setHorizontalAlignment(SwingConstants.CENTER);
        thanks.setOpaque(true);
        thanks.setVisible(false);

        add(yes);
        yes.setBounds(80, 280, 80, 40);
        yes.setBackground(salida.BLUE_COLOR);
        yes.setFont(salida.MY_FONT);
        yes.setBorder(salida.BORDER);
        yes.addMouseListener(new ChangeColorButton(yes));
        yes.addActionListener(new ShowPrompt());

        add(no);
        no.setBounds(310, 280, 80, 40);
        no.setFont(salida.MY_FONT);
        no.setBackground(salida.BLUE_COLOR);
        no.setBorder(salida.BORDER);
        no.addMouseListener(new ChangeColorButton(no));
    }


    private class ChangeColorButton implements MouseListener {
        private JButton button;

        public ChangeColorButton(JButton b){
            button = b;
        }

        @Override
        public void mouseClicked(MouseEvent e) {
            // TODO Auto-generated method stub
        }

        @Override
        public void mousePressed(MouseEvent e) {
            // TODO Auto-generated method stub 
        }

        @Override
        public void mouseReleased(MouseEvent e) {
            // TODO Auto-generated method stub
        }

        @Override
        public void mouseEntered(MouseEvent e) {
            if(button.getText().equals("NO")){
                int x_location = (int) (Math.random()*400);
                int y_location = (int) (Math.random()*350);

                button.setLocation(x_location, y_location);
            }
            else {
                button.setBackground(salida.GREEN_COLOR);  
            }
        }

        @Override
        public void mouseExited(MouseEvent e) {
            button.setBackground(salida.BLUE_COLOR);
        }    
    }

    private class ShowPrompt implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            thanks.setVisible(true);            
        }
    
        
    }
}
